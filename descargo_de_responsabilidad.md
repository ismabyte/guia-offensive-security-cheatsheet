# Leer!!!

1. Este sitio web es mi hoja de referencia personal, un documento utilizado para centralizar
   mucha información sobre técnicas de ciberseguridad, metodologías, pentesting, red team y cargas útiles.

2. No intente usar la información de este sitio web sin un consentimiento ya que podrían causar daños
   a sitios web, redes y sistemas. Recuerda que es ilegal escanear o atacar un recurso que no te pertenece y puede ser penado por la ley dependiendo tu país de residencia.

3. El contenido es una de mis guías y experiencia personal pero también de varios
   recursos en línea, hojas de trucos, y de varios hackers.

4. Este sitio web es solo para fines educativos éticos
   No tiene como objetivo armar a nadie.

5. Si tiene alguna inquietud (Algo que está mal, derechos de autor, eliminar información, etc...)
   no dude en ponerse en contacto conmigo en Twitter ([@ismabyte](https://twitter.com/@ismabyte)). Con gusto lo solucionaré a la brevedad.

6. Si este sitio te ha sido útil, retuitea/sígueme a través de Twitter ([@ismabyte](https://twitter.com/@ismabyte)). puedes compartirlo, ya que podría ayudar a otros/as también =).

7. Y es que como decía la tía de Spiderman "un gran poder lleva consigo una gran responsabilidad".

## Está Prohibido
Usted acepta no utilizar este sitio web para publicar, copiar, archivar, procesar, mostrar cualquier contenido o enlace que:

1. Tengan una conducta ilegal según las leyes de cada región.

2. Violar la privacidad de una persona u empresa.

3. Fomentar la ciberdelincuencia u otras actividades ilegales, incluidas el robo de datos, el fraude y la pirateria.

## Enlaces

El SITIO WEB pueden contener enlaces a otros portales o sitios web no administrados por ismabyte. Se declara que no se tiene control sobre dichos portales o sitios web que están sujetos a sus correspondientes condiciones de uso, y no es responsable de su contenido y en ningún caso da lugar a obligaciones de ningún tipo para nadie. Cualquier enlace que aparezca en este SITIO WEB será sólo para fines informativos y de referencia.

No acepta responsabilidad alguna, no garantiza su disponibilidad y no se responsabiliza por ninguno de sus contenidos y servicios, ni por sus prácticas de protección o confidencialidad.

El usuario hará uso de tales enlaces bajo su propia responsabilidad y para cualquier problema que pudiera surgir durante la visita o el uso de estos sitios web, deberá dirigirse al sitio web correspondiente.

## ¡Por Favor!

Abstenerse de cualquier uso ilícito, inapropiado y abusivo de los contenidos en este sitio web en todo momento.

--- 

Muchas gracias!!! Ismael


